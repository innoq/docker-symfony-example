# Docker Example mit PHP, Symfony und Mysql

Dieses Beispiel soll als einfacher Einstieg in die Entwicklung eines Docker Containers auf Basis von PHP und dem Symfony Framework dienen.
Für dieses Beispiel wird ein installierter [Docker Service](https://docs.docker.com/get-started/) benötigt, sowie das Tool [docker-compose](https://docs.docker.com/compose).

## Building

Für den initialen Start, müssen alle Image einmalig gebaut werden.
Dies lässt sich über den Befehl:

```bash
$ docker-compose build
…
```

erreichen.

## Running

Das starten aller Container erfolgt über das Kommando

```bash
$ docker-compose up
Starting docker-symfony_php_1     ... done
Starting docker-symfony_adminer_1 ... done
Starting docker-symfony_db_1      ... done
Starting docker-symfony_nginx_1   ... done
```

Die Anwendungen laufen nun alle im Vordergrund (alle Log Events werden direkt im Terminal angezeigt).
Möchte man die Service im Hintergrund starten, passiert dies über

```bash
$ docker-compose up -d
…
```

Vor dem ersten Aufruf der Anwendung, müssen allerdings noch per `composer` die Abhängigkeiten installier werden:

```bash
$ docker-compose exec php composer install
…
Generating autoload files
ocramius/package-versions:  Generating version class...
ocramius/package-versions: ...done generating version class
Executing script cache:clear [OK]
Executing script assets:install public [OK]
```

Ob alle Services nun laufen, lässt sich über folgenden Befehl überprüfen.

```bash
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS                     NAMES
9fa61964d2a1        nginx:1.15.3-alpine   "nginx -g 'daemon of…"   27 minutes ago      Up 27 minutes       0.0.0.0:10080->80/tcp     docker-symfony_nginx_1
edd2db655aa5        adminer               "entrypoint.sh docke…"   27 minutes ago      Up 27 minutes       0.0.0.0:18080->8080/tcp   docker-symfony_adminer_1
56c10b49a0fd        mysql:8.0             "docker-entrypoint.s…"   27 minutes ago      Up 27 minutes       3306/tcp, 33060/tcp       docker-symfony_db_1
3b12831d297f        docker-symfony_php    "docker-php-entrypoi…"   27 minutes ago      Up 27 minutes       9000/tcp                  docker-symfony_php_1
```

Wir können nun in einem Browser die Anwendung unter `http://localhost:10080` erreichen.

Um die Services wieder zu beenden reicht ein

```bash
$ docker-compose down
…
```

## Tools

Es werden vier einzelne Komponenten verwendet:

* Ein Container mit einer MySQL Datenbank (`db`)
* Ein Container mit einer MySQL Webadministration (`adminer`)
* Ein Container, welcher PHP 7, Symfony und die Anwendung selbst beinhaltet (`php`)
* Ein Container, in dem eine NGnix Instanz läuft, welche als Web Interface dient (`nginx`)

Der Build der einzelnen Docker Images, sowie das starten des Beispiels erfolgt per `docker-compose`.

### adminer

Die Oberfläche lässt sich über `http://localhost:18080` erreichen.  
Der Login erfolgt mit `root/root` Database: `example`

### PHP

Das `php` Image wird durch das _Dockerfile_ `docker/php/Dockerfile` erstellt.

#### Dev Setup

  Das Source Directory `apps/my-symfony-app` wird in den `php` Container gemountet. 
Änderungen aus dem lokalen Dateisystem werden ohne reload vom Container geladen.

#### Prod Setup

In Produktion sollte die Zeile

```txt
    # COPY apps/my-symfony-app /usr/src/app
```

in der Datei einkommentieren `docker/php/Dockerfile`.
Weiterhin muss der Mount aus der `docker-compose.yml` Datei auskommentiert werden.

```yaml
    volumes:
      - "./apps/my-symfony-app:/usr/src/app"
```

### Symfony Composer

Alle _Composer_ Kommandos lassen sich direkt über den Container ausführen (`docker-compose exec php composer CMD`).

```bash
$ docker-compose exec php composer create-project symfony/website-skeleton app
…
$ docker-compose exec php composer install
Do not run Composer as root/super user! See https://getcomposer.org/root for details
Loading composer repositories with package information
Updating dependencies (including require-dev)
Package operations: 1 install, 0 updates, 0 removals
As there is no 'unzip' command installed zip files are being unpacked using the PHP zip extension.
This may cause invalid reports of corrupted archives. Besides, any UNIX permissions (e.g. executable) defined in the archives will be lost.
Installing 'unzip' may remediate them.
  - Installing monolog/monolog (1.0.2): Downloading (100%)
Writing lock file
Generating autoload files

$ docker-compose exec php php bin/console doctrine:schema:create
…
$ docker-compose exec php php bin/console doctrine:fixtures:load
…
$ docker-compose exec php php bin/console assets:install --symlink public/
…
```

### Nginx

Die Konfiguration des Nginx Services befindet sich in `docker/nginx/default.conf`.  
Der Nginx ist erreichbar unter `http://localhost:10080`

